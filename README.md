# **High Concept: A queda de Seth**
Felippe Matheo Marquesin, Lucas Ferreira Denipoti, Lucas Idacir Sbrugnera Nazato, Stephanny da Silva Nascimento

matheofelippe@gmail.com, lucasfdenipoti@gmail.com, lucas.nazato@hotmail.com, stephanny.nascimento08@gmail.com

Departamento de Jogos Digitais - Centro Universitário Facens (UniFACENS) - Caixa Postal 355 e 664 – Sorocaba– SP – Brasil 

 
## 1. Descrição
Jogo de tiro/ação 3D com uma visão de terceira pessoa, com ambientação no deserto. O jogador faz uso de habilidades mágicas para combate e tem a missão de salvar o povo do mal governo de seu próprio pai Seth.
 
## 2. Gênero
Ação / Aventura em Terceira Pessoa.
 
## 3. Principais Mecânicas
## 3.1 Gameplay
O jogador irá enfrentar hordas de inimigos e solucionar quebra-cabeças para prosseguir nos níveis do jogo.

## 3.2 Condição de vitória 
Derrotar todos os inimigos na fase e completar os objetivos para seguir em frente.

## 3.3 Player 
Stats: Vida e Dano.

## 3.4 Inimigos - IA
Khaimera, ela é instanciada no cenário para perseguir e abater o player.

## 4. História 
O Deus Seth tomou controle do Egito Antigo e controla as terras como um tirano, obrigando os seus servos humanos a trabalharem exaustivamente. Você joga como Thema, filho do Deus Seth, e não concorda com as práticas do seu pai e busca a ajuda dos outros deuses para libertar a população.
 
## 5. Diferencial 
Temática do Egito Antigo (Mitologia/Deuses Egípcios) misturada com gênero de tiro/ação FPS.
 
## 6. Sistema Operacional 
PC
Mobile(Android).
 
## 7. Público Alvo
Homens e mulheres de 18 a 35 anos.
 
## 8. Equipe para Desenvolvimento 
1 Gerente de Projeto,
2 Programadores,
1 Artista 3D,
1 Artista de Interface.
 
## 9. Orçamento 
Aproximadamente 20 horas semanais. 
Salário mínimo: R$ 44,00/hora * 4 = R$ 176,00/hora + (Custos adicionais) => R$ 200,00/hora 
Aproximadamente 10 semanas. 
R$ 1000,00 por semana * 10 semanas = R$ 10.000,00/pessoa 
R$ 10.000,00 * 4 pessoas = R$ 40.000,00 
 
Orçamento Final: R$ 40.000,00.


 
---
 


# **Game Design Document - A Queda de Seth**



<div align="center">

![ALT](https://gitlab.com/lucasnazato/gdd_seth/-/raw/main/GDDImages/a-queda-de-seth-banner.jpg)

Para PC (GameJolt)

**Classificação:** ![ALT](https://gitlab.com/lucasnazato/gdd_seth/-/raw/main/GDDImages/ClassInd_14.png)

**Data de lançamento:** 06/06/2022

F2LTeam / f2lteamgames@gmail.com
</div>

## História do jogo:

O Deus Seth tomou controle do Egito Antigo e controla as terras como um tirano, obrigando os seus servos humanos a trabalharem exaustivamente. Você joga como Thema, filho do Deus Set, e não concorda com as práticas do seu pai e busca a ajuda dos outros deuses para libertar a população.

## O jogo:
Em A Queda de Seth, o jogador toma controle de Thema, lutando contra os sacerdotes de seu pai, o Deus Seth, enquanto progride pelo seu templo. O jogo é separado em duas fases distintas para que o jogador se acostume com as mecânicas.

Na primeira o foco é dado para a movimentação do personagem e suas habilidades e como isso pode ser usado para progredir pelo templo, como coleta de itens para abrir passagens e obstáculos.

Na segunda fase o jogador é introduzido ao combate do jogo contra inimigos, em um sistema de hordas, onde precisa sobreviver ao desafio para que possa progredir. São também introduzidas novas mecânicas de se locomover pelo level.

## Ritmo:
Exploração de templos egípcios com combate usando magia.

<div align="center">

[cena de gameplay?]
</div>


## Personagem do jogador
**Thema:** Conforme o player avança no jogo e ganha o apoio de diferentes deuses, ele passa a ter acesso a diferentes habilidades que podem ajudar tanto no combate quanto na exploração do templo de seu pai. Neste protótipo o jogador já inicia com as habilidades básicas de movimento e um único ataque.

<div align="center">

![ALT](https://gitlab.com/lucasnazato/gdd_seth/-/raw/main/GDDImages/Thema_personagem.png)
</div>


## Controles do jogador:

| Comandos | Keyboard/Mouse | Controle (XBOX) |
| ------ | ------ | ------ |
| Mover personagem | WASD | Joystick esquerdo |
| Mover câmera | Mouse | Joystick direito |
| Pular | Barra de espaço | A |
| Correr | Shift esquerdo | B |
| Atirar | Botão esquerdo do mouse | X |

## Mundo do jogo:
O mundo de A Queda de Seth é inspirado na mitologia egípcia e portanto os seus visuais, level design e mecânicas são todos baseados em diferentes aspectos  desta cultura. A principal localização que o jogador explora é o tempo do Deus Seth, que se encontra no meio do deserto. Conforme o jogador progride pelo templo armadilhas e inimigos mais perigosos aparecem.
 
## Experiência de jogo:
O jogo é dividido em níveis para exploração, sendo que cada um destes é “dividido” em diferentes fases, Cutscene / Exploração / Puzzle / Combate, que podem estar em ordens diferentes.

**Cutscene** - O jogador recebe uma breve explicação da história e um objetivo que precisa ser cumprido.

**Exploração** - Percorrer o templo para achar itens de vida e dano, entre outras recompensas.

**Puzzle** - Em certos pontos o jogador irá precisar resolver puzzles ou achar os itens corretos para que ele possa progredir no nível.

**Combate** - Inimigos estão espalhados pelos níveis, podendo ser combates opcionais ou obrigatórios para a progressão.

**Como vencer** - Atravessar o templo e derrotar o Deus Seth.

**Como perder** - Ser derrotado por algum inimigo ou armadilha.

## Mecânicas de jogo
A Queda de Seth é um jogo em terceira pessoa, com a câmera posicionada atrás do personagem, podendo ser rotacionada em 360 graus. O jogador percorre o templo batalhando contra inimigos. As ações realizadas pelo jogador são as de coletar itens de melhoria (vida e dano) e itens de progressão (abrir portas), desviar de armadilhas, atirar magia para derrotar inimigos e interagir com o ambiente.

A HUD mostra ao jogador a sua vida e dano, que são mantidas de um nível para o outro, e pode mostrar informações contextuais ao modo de jogo do momento, como o sistema de hordas, onde as ondas e o tempo de início destas são destacados.

## Power-ups
O jogador pode coletar dois tipos de power-up:

**Ankh Dourado:** Aumenta o dano causado pelo personagem.

**Ankh Prata:** Recupera a vida do personagem.

<div align="center">

![ALT](https://gitlab.com/lucasnazato/gdd_seth/-/raw/main/GDDImages/Itens.png)
</div>

## Inimigos:
Os inimigos presentes no templo são os sacerdotes de Seth, que podem ter habilidades diferentes.

**Melee:** Este inimigo persegue o jogador e quando se aproxima ataca usando o seu machado.

**Ranged:** Se posiciona de forma que o jogador esteja no seu campo de visão e utiliza ataques a distância.

<div align="center">

![ALT](https://gitlab.com/lucasnazato/gdd_seth/-/raw/main/GDDImages/khaimera.jpg)
</div>

## Cenas de corte:
As cenas de corte serão feitas dentro jogo, usando itens/NPCs para expor a história e dar objetivos ao jogador.

















